package ru.kazakov.iteco.repository;

import ru.kazakov.iteco.entity.User;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class UserRepository extends AbstractRepository<User> {

    private final Map<String, User> entities = new LinkedHashMap<>();

    public String getName(String id) {
        return entities.get(id).getName();
    }

    public void setName(String name, String id) {
        entities.get(id).setName(name);
    }

    @Override
    public void merge(User entity) {
        entities.merge(entity.getId(), entity, (oldVal, newVal) -> newVal);
    }

    @Override
    public void persist(User entity) { entities.putIfAbsent(entity.getId(), entity);}

    @Override
    public void remove(String id) { entities.remove(id);}

    @Override
    public void removeAll() { entities.clear();}

    @Override
    public User findOne(String id) {
        return entities.get(id);
    }

    public User findByLogin(String login) {
        List<User> list = new ArrayList<>(entities.values());
        return list.stream()
                .filter(v -> v.getLogin().equals(login))
                .findFirst().orElse(null);
    }

    @Override
    public List<User> findAll() {
        return new ArrayList<>(entities.values());
    }

    public boolean contains(String login) {
        List<User> list = new ArrayList<>(entities.values());
        return list.stream()
                .anyMatch(v -> v.getLogin().equals(login));
    }

    public boolean isEmpty() { return entities.isEmpty();}

}
