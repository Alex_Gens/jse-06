package ru.kazakov.iteco.repository;

import ru.kazakov.iteco.entity.Task;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractRepository<Task> {

    private final Map<String, Task> entities = new LinkedHashMap<>();

    public String getName(String id) {
        return entities.get(id).getName();
    }

    public void setName(String name, String id) {
        entities.get(id).setName(name);
    }

    public String getProject(String id) {
        return entities.get(id).getProject();
    }

    public void setProject(String id, String projectId) {
        entities.get(id).setProject(projectId);
    }

    @Override
    public void merge(Task entity) {
        entities.merge(entity.getId(), entity, (v1, v2)
                -> {v1.setInfo(v2.getInfo());
                return v1;});
    }

    @Override
    public void persist(Task entity) {
        entities.putIfAbsent(entity.getId(), entity);
    }

    @Override
    public void remove(String id) {
        entities.remove(id);
    }

    public void remove(List<String> ids) {
        entities.entrySet().removeIf(entry
                -> ids.contains(entry.getValue().getId()));
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    public void removeAll(String currentUserId) {
        entities.entrySet().removeIf(entry -> entry.getValue().getUserId().equals(currentUserId));
    }

    @Override
    public Task findOne(String id) {
        return entities.get(id);
    }

    public Task findByName(String name) {
        List<Task> list = new ArrayList<>(entities.values());
        return list.stream()
                   .filter(v -> v.getName().equals(name))
                   .findFirst().orElse(null);
    }

    public Task findByName(String name, String currentUserId) {
        List<Task> list = new ArrayList<>(entities.values());
        return list.stream()
                   .filter(v -> v.getName().equals(name) && v.getUserId().equals(currentUserId))
                   .findFirst().orElse(null);
    }

    @Override
    public List<Task> findAll() {
        return new ArrayList<>(entities.values());
    }

    public List<Task> findAll(List<String> ids) {
        List<Task> list = new ArrayList<>(entities.values());
        return  list.stream()
                    .filter( v -> ids.contains(v.getId()))
                    .collect(Collectors.toList());
    }

    public List<Task> findAll(String currentUserId) {
        List<Task> list = new ArrayList<>(entities.values());
        return  list.stream()
                    .filter( v -> v.getUserId().equals(currentUserId))
                    .collect(Collectors.toList());

    }

    public boolean contains(String name) {
        List<Task> list = new ArrayList<>(entities.values());
        return list.stream()
                   .anyMatch(v -> v.getName().equals(name));
    }

    public boolean contains(String name, String currentUserId) {
        List<Task> list = new ArrayList<>(entities.values());
        return list.stream()
                   .anyMatch(v -> v.getName().equals(name) && v.getUserId().equals(currentUserId));
    }

    public boolean isEmpty() {
        return entities.isEmpty();
    }

    public boolean isEmpty(String id) {
        return entities.get(id).isEmpty();
    }

}
