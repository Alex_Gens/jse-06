package ru.kazakov.iteco.repository;

import ru.kazakov.iteco.entity.Project;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ProjectRepository extends AbstractRepository<Project> {

    private final Map<String, Project> entities = new LinkedHashMap<>();

    public String getName(String id) {
        return entities.get(id).getName();
    }

    public void setName(String name, String id) {
        entities.get(id).setName(name);
    }

    @Override
    public void merge(Project entity) {
        entities.merge(entity.getId(), entity, (v1, v2)
                -> {v1.setInfo(v2.getInfo());
                    return v1;});
    }

    @Override
    public void persist(Project entity) {
        entities.putIfAbsent(entity.getId(), entity);
    }

    @Override
    public void remove(String id) {
        entities.remove(id);
    }

    public void remove(List<String> ids) {
        entities.entrySet().removeIf(entry
                -> ids.contains(entry.getValue().getId()));
    }

    @Override
    public void removeAll() {entities.clear(); }

    public void removeAll(String currentUserId) {
        entities.entrySet().removeIf(entry -> entry.getValue().getUserId().equals(currentUserId));
    }

    @Override
    public Project findOne(String id) {
        return entities.get(id);
    }

    public Project findByName(String name) {
        List<Project> list = (List<Project>) entities.values();
        return list.stream()
                   .filter(v -> v.getName().equals(name))
                   .findFirst().orElse(null);
    }

    public Project findByName(String name, String currentUserId) {
        List<Project> list = (List<Project>) entities.values();
        return list.stream()
                   .filter(v -> v.getName().equals(name) && v.getUserId().equals(currentUserId))
                   .findFirst().orElse(null);
    }

    @Override
    public List<Project> findAll() {
        return new ArrayList<>(entities.values());
    }

    public List<Project> findAll(List<String> ids) {
        List<Project> list = new ArrayList<>(entities.values());
        return  list.stream()
                    .filter( v -> ids.contains(v.getId()))
                    .collect(Collectors.toList());
    }

    public List<Project> findAll(String currentUserId) {
        List<Project> list = new ArrayList<>(entities.values());
        return  list.stream()
                    .filter( v -> v.getUserId().equals(currentUserId))
                    .collect(Collectors.toList());

    }

    public boolean contains(String name) {
        List<Project> list = new ArrayList<>(entities.values());
        return list.stream()
                   .anyMatch(v -> v.getName().equals(name));
    }

    public boolean contains(String name, String currentUserId) {
        List<Project> list = new ArrayList<>(entities.values());
        return list.stream()
                .anyMatch(v -> v.getName().equals(name) && v.getUserId().equals(currentUserId));
    }

    public boolean isEmpty() {
        return entities.isEmpty();
    }

    public boolean isEmpty(String id) {
        return entities.get(id).isEmpty();
    }

}
