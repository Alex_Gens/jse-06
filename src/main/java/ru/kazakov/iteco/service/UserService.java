package ru.kazakov.iteco.service;

import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.repository.UserRepository;
import java.util.List;

public class UserService extends AbstractService<User> {

    UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String getName(String id) {
        return userRepository.getName(id);
    }

    public void setName(String name, String id) {
        userRepository.setName(name, id);
    }

    @Override
    public void merge(User entity) throws Exception {
        if (entity == null) throw new Exception();
        userRepository.merge(entity);
    }

    @Override
    public void persist(User entity) throws Exception {
        if (entity == null) throw new Exception();
        userRepository.persist(entity);
    }

    @Override
    public void remove(String id) throws Exception {
        if (id == null || id.isEmpty()) { throw new Exception();}
        userRepository.remove(id);
    }

    @Override
    public void removeAll() {
        userRepository.removeAll();
    }

    @Override
    public User findOne(String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return userRepository.findOne(id);
    }

    public User findByLogin(String login) throws Exception {
        if (login == null || login.isEmpty()) {throw new Exception();}
        return userRepository.findByLogin(login);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    public boolean contains(String login) throws Exception {
        if (login == null || login.isEmpty()) {throw new Exception();}
        return userRepository.contains(login);
    }

    @Override
    public boolean isEmpty() {
        return userRepository.isEmpty();
    }

}
