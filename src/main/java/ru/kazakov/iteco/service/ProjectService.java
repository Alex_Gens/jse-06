package ru.kazakov.iteco.service;

import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.repository.ProjectRepository;
import java.util.List;

public class ProjectService extends AbstractService<Project> {

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public String getName(String id) throws Exception {
        if (id == null || id.isEmpty()) {throw new Exception();}
        return projectRepository.getName(id);
    }

    public void setName(String name, String id) throws Exception {
        if (name == null || name.isEmpty()) {throw new Exception();}
        if (id == null || id.isEmpty()) {throw new Exception();}
        projectRepository.setName(name, id);
    }

    @Override
    public void merge(Project entity) throws Exception {
        if (entity == null) { throw new Exception();}
        projectRepository.merge(entity);
    }

    @Override
    public void persist(Project entity) throws Exception {
        if (entity == null) {throw new Exception(); }
        projectRepository.persist(entity);
    }

    public void persist(String name) throws Exception {
        if (name == null || name.isEmpty()) {throw new Exception();}
        Project project = new Project(name);
        projectRepository.persist(project);
    }

    @Override
    public void remove(String id) throws Exception {
        if (id == null || id.isEmpty()) {throw new Exception();}
        projectRepository.remove(id);
    }

    public void remove(List<String> ids) throws Exception {
        if (ids == null) { throw new Exception();}
        projectRepository.remove(ids);
    }

    @Override
    public void removeAll() {
        projectRepository.removeAll();
    }

    public void removeAll(String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) {throw new Exception();}
        projectRepository.removeAll(currentUserId);
    }

    @Override
    public Project findOne(String id) throws Exception {
        if (id == null || id.isEmpty()) {throw new Exception();}
        return projectRepository.findOne(id);
    }

    public Project findByName(String name) throws Exception {
        if (name == null || name.isEmpty()) {throw new Exception();}
        return projectRepository.findByName(name);
    }

    public Project findByName(String name, String currentUserId) throws Exception {
        if (name == null || name.isEmpty()) {throw new Exception();}
        if (currentUserId == null || currentUserId.isEmpty()) {throw new Exception();}
        return projectRepository.findByName(name, currentUserId);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public List<Project> findAll(List<String> ids) throws Exception {
        if (ids == null) { throw new Exception();}
        return projectRepository.findAll(ids);
    }

    public List<Project> findAll(String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) {throw new Exception();}
        return projectRepository.findAll(currentUserId);
    }

    public boolean contains(String name) throws Exception {
        if (name == null || name.isEmpty()) {throw new Exception();}
        return projectRepository.contains(name);
    }

    public boolean contains(String name, String currentUserId) throws Exception {
        if (name == null || name.isEmpty()) {throw new Exception();}
        if (currentUserId == null || currentUserId.isEmpty()) {throw new Exception();}
        return projectRepository.contains(name, currentUserId);
    }

    @Override
    public boolean isEmpty() {
        return projectRepository.isEmpty();
    }

    public boolean isEmpty(String id) throws Exception {
        if (id == null || id.isEmpty()) {throw new Exception();}
        return projectRepository.isEmpty(id);
    }

}
