package ru.kazakov.iteco.view;

import ru.kazakov.iteco.enumeration.EntityType;
import ru.kazakov.iteco.util.ConsoleReader;
import java.io.IOException;

public class ConsoleWriter {

    public final String separator = System.lineSeparator();

    private final ConsoleReader reader = new ConsoleReader();

    public String enterEntityName(EntityType entityType) throws IOException {
        System.out.println("ENTER " + entityType.getValue().toUpperCase() + " NAME:");
        return reader.enterIgnoreEmpty();
    }

    public void separateLines() {
        System.out.print(separator);
    }

}
