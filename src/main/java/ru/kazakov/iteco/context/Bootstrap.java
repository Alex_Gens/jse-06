package ru.kazakov.iteco.context;

import ru.kazakov.iteco.command.*;
import ru.kazakov.iteco.command.project.*;
import ru.kazakov.iteco.command.system.ExitCommand;
import ru.kazakov.iteco.command.system.HelpCommand;
import ru.kazakov.iteco.command.task.*;
import ru.kazakov.iteco.command.user.*;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.repository.UserRepository;
import ru.kazakov.iteco.service.UserService;
import ru.kazakov.iteco.util.ConsoleReader;
import ru.kazakov.iteco.repository.ProjectRepository;
import ru.kazakov.iteco.repository.TaskRepository;
import ru.kazakov.iteco.service.ProjectService;
import ru.kazakov.iteco.service.TaskService;
import ru.kazakov.iteco.util.Password;
import ru.kazakov.iteco.view.ConsoleWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Bootstrap {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private final TaskRepository taskRepository = new TaskRepository();
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final TaskService taskService = new TaskService(taskRepository);
    private final ProjectService projectService = new ProjectService(projectRepository);
    private final UserRepository userRepository = new UserRepository();
    private final UserService userService = new UserService(userRepository);
    private final ConsoleReader reader = new ConsoleReader();
    private final ConsoleWriter writer = new ConsoleWriter();
    private User currentUser = null;

    {
        registry(new ProjectCreateCommand(this));
        registry(new ProjectGetCommand(this));
        registry(new ProjectUpdateCommand(this));
        registry(new ProjectRemoveCommand(this));
        registry(new ProjectListCommand(this));
        registry(new ProjectAddTaskCommand(this));
        registry(new ProjectListTasksCommand(this));
        registry(new ProjectClearCommand(this));
        registry(new TaskCreateCommand(this));
        registry(new TaskGetCommand(this));
        registry(new TaskUpdateCommand(this));
        registry(new TaskRemoveCommand(this));
        registry(new TaskListCommand(this));
        registry(new TaskClearCommand(this));
        registry(new UserLoginCommand(this));
        registry(new UserChangePasswordCommand(this));
        registry(new UserLogoutCommand(this));
        registry(new UserGetCommand(this));
        registry(new UserCreateCommand(this));
        registry(new UserUpdateCommand(this));
        registry(new UserListCommand(this));
        registry(new HelpCommand(this));
        registry(new ExitCommand(this));
        try {
            addUsers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() throws Exception {
        String command = "";
        while (true) {
            if (currentUser == null) {
                System.out.println("You are not authorized. Use \"user-login\" for authorization.");
                writer.separateLines();
            }
            command = reader.enterIgnoreEmpty();
            command = command.trim().toLowerCase();
            execute(command);
        }
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public UserService getUserService() { return userService;}

    public User getCurrentUser() { return currentUser;}

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public List<AbstractCommand> getCommands() { return new ArrayList<>(commands.values());}

    private void registry(AbstractCommand command) {
        commands.put(command.getName(), command);
    }

    private void execute(final String command) throws Exception {
        private void execute(final String command) throws Exception {
            if (command == null || command.isEmpty()) return;
            if (!commands.containsKey(command)) {
                System.out.println("Command \"" + command +  "\" doesn't exist. Use \"help\" to show all commands.");
                writer.separateLines();
                return;
            }
            final AbstractCommand abstractCommand = commands.get(command);
            if (currentUser == null && abstractCommand.isAdmin()) {
                System.out.println("Command \"" + command +  "\" doesn't exist. Use \"help\" to show all commands.");
                return;
            }
            if (currentUser == null && abstractCommand.isSecure()) {
                System.out.println("[NO ACCESS]");
                return;
            }
            if (currentUser == null && !abstractCommand.isSecure()) {
                abstractCommand.execute();
                return;
            }
            if (currentUser.getRoleType() != RoleType.ADMINISTRATOR && abstractCommand.isAdmin()) {
                System.out.println("Command \"" + command +  "\" doesn't exist. Use \"help\" to show all commands.");
                return;
            }
            abstractCommand.execute();
        }

    private void addUsers() throws Exception {
        User user = new User();
        user.setLogin("user");
        String password = Password.getHashedPassword("pass");
        user.setPassword(password);
        user.setRoleType(RoleType.DEFAULT);
        User admin = new User();
        admin.setLogin("admin");
        admin.setPassword(password);
        admin.setRoleType(RoleType.ADMINISTRATOR);
        userService.persist(user);
        userService.persist(admin);
    }

}
