package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.service.TaskService;
import java.util.ArrayList;
import java.util.List;

public class ProjectListTasksCommand extends ProjectAbstractCommand {

    private final String name = "project-list-tasks";

    private final String description = "Show all tasks in project.";

    public ProjectListTasksCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        String projectName = writer.enterEntityName(entityType);
        if (!projectService.contains(projectName, currentUserId)) {
            System.out.println("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
            writer.separateLines();
            return;
        }
        TaskService taskService = bootstrap.getTaskService();
        Project project = projectService.findByName(projectName, currentUserId);
        List<String> taskIds = new ArrayList<>();
        for (Task task : taskService.findAll(currentUserId)
        ) {
            if (task.getProject().equals(project.getId())) {
                taskIds.add(task.getId());
            }
        }
        if (taskIds.isEmpty()) {
            System.out.println("The project has no tasks. Use project-add-task to add task to project.");
            writer.separateLines();
            return;
        }
        int counter = 1;
        System.out.println("[TASKS LIST]");
        for (String taskId : taskIds
        ) {
            System.out.println(counter + ": " + taskService.getName(taskId));
            counter++;
        }
        writer.separateLines();
    }

}
