package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.service.TaskService;
import java.util.ArrayList;
import java.util.List;

public class ProjectRemoveCommand extends ProjectAbstractCommand {

    private final String name = "project-remove";

    private final String description = "Remove project.";

    public ProjectRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        String name = writer.enterEntityName(entityType);
        if (!projectService.contains(name, currentUserId)) {
            System.out.println("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
            writer.separateLines();
            return;
        }
        Project project = projectService.findByName(name, currentUserId);
        TaskService taskService = bootstrap.getTaskService();
        List<String> taskIds = new ArrayList<>();
        for ( Task task : taskService.findAll(currentUserId)
        ) {
            if (task.getProject().equals(project.getId())) {
                taskIds.add(task.getId());
            }
        }
        taskService.remove(taskIds);
        projectService.remove(project.getId());
        System.out.println("[REMOVED]");
        System.out.println("Project successfully removed!");
        writer.separateLines();
    }

}
