package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Project;

public class ProjectCreateCommand extends ProjectAbstractCommand {

    private final String name = "project-create";

    private final String description = "Create new project.";

    public ProjectCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() { return description;}

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        String name = writer.enterEntityName(entityType);
        if (projectService.contains(name, currentUserId)) {
            System.out.println("[NOT CREATED]");
            System.out.println("Project with this name is already exists. Use another project name.");
            writer.separateLines();
            return;
        }
        Project project = new Project(name);
        project.setUserId(currentUserId);
        projectService.persist(project);
        System.out.println("[CREATED]");
        System.out.println("Project successfully created!");
        writer.separateLines();
    }

}
