package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.service.TaskService;
import java.util.ArrayList;
import java.util.List;

public class ProjectClearCommand extends ProjectAbstractCommand {

    private final String name = "project-clear";

    private final String description = "Remove all projects.";

    public ProjectClearCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        List<String> taskIds = new ArrayList<>();
        List<String> projectIds = new ArrayList<>();
        for (Project project : projectService.findAll(currentUserId)) {
            projectIds.add(project.getId());
        }
        TaskService taskService = bootstrap.getTaskService();
        for (Task task : taskService.findAll(currentUserId)) {
            if (projectIds.contains(task.getProject())) {
                taskIds.add(task.getId());
            }
        }
        taskService.remove(taskIds);
        projectService.remove(projectIds);
        System.out.println("[ALL PROJECTS REMOVED]");
        System.out.println("Projects successfully removed!");
        writer.separateLines();
    }

}
