package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Project;
import java.util.List;

public class ProjectListCommand extends ProjectAbstractCommand {

    private final String name = "project-list";

    private final String description = "Show all projects.";

    public ProjectListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        List<Project> projects = projectService.findAll(currentUserId);
        if (projects == null || projects.isEmpty()) {
            System.out.println("Project list is empty. Use \"project-create\" to create project.");
            writer.separateLines();
            return;
        }
        System.out.println("[PROJECTS LIST]");
        int counter = 1;
        for (Project project : projects) {
            System.out.println(counter + ". " + project.getName());
            counter++;
        }
        writer.separateLines();
    }

}
