package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.EntityType;
import ru.kazakov.iteco.service.TaskService;

public class ProjectAddTaskCommand extends ProjectAbstractCommand {

    private final String name = "project-add-task";

    private final String description = "Add task to project.";

    public ProjectAddTaskCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        String projectName = writer.enterEntityName(entityType);
        if (!projectService.contains(projectName, currentUserId)) {
            System.out.println("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
            writer.separateLines();
            return;
        }
        Project project = projectService.findByName(projectName,currentUserId );
        TaskService taskService = bootstrap.getTaskService();
        String taskName = writer.enterEntityName(EntityType.TASK);
        if (!taskService.contains(taskName, currentUserId)) {
            System.out.println("Task with this name doesn't exist. Use \"task-list\" to show all tasks.");
            writer.separateLines();
            return;
        }
        Task task = taskService.findByName(taskName,currentUserId);
        if (task.getProject().equals(project.getId())) {
            System.out.println("Task is already added. Use project-list-tasks to see tasks in project.");
            writer.separateLines();
            return;
        }
        task.setProject(project.getId());
        System.out.println("Task successfully added!");
        writer.separateLines();
    }

}
