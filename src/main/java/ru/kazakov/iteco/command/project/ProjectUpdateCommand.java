package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.util.ConsoleReader;

public class ProjectUpdateCommand extends ProjectAbstractCommand {

    private final ConsoleReader reader = new ConsoleReader();

    private final String name = "project-update";

    private final String description = "Update project information.";

    public ProjectUpdateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        String name = writer.enterEntityName(entityType);
        if (!projectService.contains(name, currentUserId)) {
            System.out.println("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
            writer.separateLines();
            return;
        }
        Project project = projectService.findByName(name, currentUserId);
        System.out.println("Use \"-save\" to finish entering, and save information.");
        System.out.println("ENTER PROJECT INFORMATION");
        String newInfo = reader.read();
        project.setInfo(newInfo);
        System.out.println("[UPDATED]");
        System.out.println("Project successfully updated!");
        writer.separateLines();
    }

}
