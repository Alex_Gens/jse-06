package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.command.AbstractCommand;
import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.enumeration.EntityType;
import ru.kazakov.iteco.service.ProjectService;
import ru.kazakov.iteco.view.ConsoleWriter;

public abstract class ProjectAbstractCommand extends AbstractCommand {

    final EntityType entityType = EntityType.PROJECT;

    final ConsoleWriter writer = new ConsoleWriter();

    final ProjectService projectService = bootstrap.getProjectService();

    String currentUserId = "";

    public ProjectAbstractCommand(Bootstrap bootstrap) {super(bootstrap);}

    public void execute() throws Exception {
        String currentUserId = bootstrap.getCurrentUser().getId();
        if (currentUserId == null || currentUserId.isEmpty()) {
            throw new Exception();
        }
        this.currentUserId = currentUserId;
    }

}
