package ru.kazakov.iteco.command.project;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Project;

public class ProjectGetCommand extends ProjectAbstractCommand {

    private final String name = "project-get";

    private final String description = "Show all project information.";

    public ProjectGetCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        String name = writer.enterEntityName(entityType);
        if (!projectService.contains(name, currentUserId)) {
            System.out.println("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
            writer.separateLines();
            return;
        }
        Project project = projectService.findByName(name, currentUserId);
        if (projectService.isEmpty(project.getId())) {
            System.out.println("Project is empty. Use \"project-update\" to update this project.");
            writer.separateLines();
            return;
        }
        System.out.println("[Project: " + name + "]");
        System.out.println(project.getInfo());
        writer.separateLines();
    }

}
