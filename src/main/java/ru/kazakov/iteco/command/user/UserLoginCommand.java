package ru.kazakov.iteco.command.user;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.util.ConsoleReader;
import ru.kazakov.iteco.util.Password;

public class UserLoginCommand extends UserAbstractCommand{

    private final String name = "user-login";

    private final String description = "User authorization.";

    private final ConsoleReader reader = new ConsoleReader();



    public UserLoginCommand(Bootstrap bootstrap) {
        super(bootstrap);
        this.secure = false;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER LOGIN: ");
        String login = reader.enterIgnoreEmpty();
        if (!userService.contains(login)) {
            System.out.println("[NOT CORRECT]");
            System.out.println("Login doesn't exist.");
            writer.separateLines();
            return;
        }
        User currentUser = bootstrap.getCurrentUser();
        if (currentUser != null &&
                login.equals(bootstrap.getCurrentUser().getLogin())) {
            System.out.println("[NOT CORRECT]");
            System.out.println("You are already authorized.");
            writer.separateLines();
            return;
        }
        System.out.println("[CORRECT]");
        writer.separateLines();
        User user = userService.findByLogin(login);
        System.out.println("ENTER PASSWORD: ");
        String enteredPassword = reader.enterIgnoreEmpty();
        String password = Password.getHashedPassword(enteredPassword);
        if (!user.getPassword().equals(password)) {
            System.out.println("[NOT CORRECT]");
            System.out.println("Incorrect password.");
            writer.separateLines();
            return;
        }
        System.out.println("[CORRECT]");
        bootstrap.setCurrentUser(user);
        System.out.println("Welcome " + user.getLogin() + "!" + " ["
                + user.getRoleType().getName().toUpperCase() + "]");
        writer.separateLines();
    }

}
