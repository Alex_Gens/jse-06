package ru.kazakov.iteco.command.user;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.util.ConsoleReader;
import ru.kazakov.iteco.util.Password;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class UserChangePasswordCommand extends UserAbstractCommand {

    private final String name = "user-change-password";

    private final String description = "Change profile password.";

    private final ConsoleReader reader = new ConsoleReader();

    public UserChangePasswordCommand(Bootstrap bootstrap) { super(bootstrap);}

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        User currentUser = bootstrap.getCurrentUser();
        User user = bootstrap.getCurrentUser();
        if (currentUser.getRoleType() == RoleType.ADMINISTRATOR) {
            System.out.println("Enter user's login to change profile password.   [" + user.getRoleType().getName().toUpperCase() + "]");
            System.out.println("ENTER LOGIN: ");
            String login = reader.enterIgnoreEmpty();
            boolean isExist = userService.contains(login);
            if (!isExist) {
                System.out.println("[NOT CORRECT]");
                System.out.println("User with that login doesn't exist.");
                writer.separateLines();
                return;
            }
            user = userService.findByLogin(login);
        }

        if (currentUser.getRoleType() != RoleType.ADMINISTRATOR) {
            checkOldPassword(user);
        }
        String firstPassword = "";
        String secondPassword = "";
        while (true) {
            System.out.println("Enter new password: ");
            firstPassword = reader.enterIgnoreEmpty();
            System.out.println("Confirm you password.");
            System.out.println("Enter new password: ");
            secondPassword = reader.enterIgnoreEmpty();
            if (!firstPassword.equals(secondPassword)) {
                System.out.println("[NOT UPDATED]");
                System.out.println("Entered passwords are different.");
                writer.separateLines();
                continue;
            }
            System.out.println("[UPDATED]");
            break;
        }
        System.out.println("Password updated!");
        writer.separateLines();
        String newPassword = Password.getHashedPassword(firstPassword);
        user.setPassword(newPassword);
    }

    private void checkOldPassword(User user) throws IOException, NoSuchAlgorithmException {
        System.out.println("Enter password: ");
        String entered = reader.enterIgnoreEmpty();
        String enteredPassword = Password.getHashedPassword(entered);
        String oldPassword = user.getPassword();
        if (!enteredPassword.equals(oldPassword)) {
            System.out.println("[NOT CORRECT]");
            writer.separateLines();
        }
    }

}
