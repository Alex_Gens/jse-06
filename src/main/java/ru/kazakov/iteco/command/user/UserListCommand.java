package ru.kazakov.iteco.command.user;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.util.ConsoleReader;
import java.util.List;

public class UserListCommand extends  UserAbstractCommand {

    private final String name = "user-list";

    private final String description = "Show all users.   [" + RoleType.ADMINISTRATOR.getName().toUpperCase() + "]";

    private final ConsoleReader reader = new ConsoleReader();

    public UserListCommand(Bootstrap bootstrap) {
        super(bootstrap);
        this.admin = true;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        List<User> users = userService.findAll();
        if (users == null || users.isEmpty()) {
            System.out.println("User list is empty.");
            writer.separateLines();
            return;
        }
        System.out.println("[USERS LIST]");
        int counter = 1;
        for (User user : users) {
            System.out.println(counter + ". " + user.getLogin());
            counter++;
        }
        writer.separateLines();
    }

}
