package ru.kazakov.iteco.command.user;

import ru.kazakov.iteco.context.Bootstrap;

public class UserLogoutCommand extends UserAbstractCommand {

    private final String name = "user-logout";

    private final String description = "Logout from account.";

    public UserLogoutCommand(Bootstrap bootstrap) {super(bootstrap); }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        bootstrap.setCurrentUser(null);
        System.out.println("[LOGOUT]");
        System.out.println("You logout from your account.");;
        writer.separateLines();
    }

}
