package ru.kazakov.iteco.command.user;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.util.ConsoleReader;
import ru.kazakov.iteco.util.Format;
import java.util.Date;

public class UserGetCommand extends UserAbstractCommand {

    private final String name = "user-get";

    private final String description = "Show user profile information.";

    private final ConsoleReader reader = new ConsoleReader();

    public UserGetCommand(Bootstrap bootstrap) {super(bootstrap); }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        User user = bootstrap.getCurrentUser();
        if (user.getRoleType() == RoleType.ADMINISTRATOR) {
            System.out.println("Enter user's login to get user's profile.   [" + user.getRoleType().getName().toUpperCase() + "]");
            System.out.println("ENTER LOGIN: ");
            String login = reader.enterIgnoreEmpty();
            boolean isExist = userService.contains(login);
            if (!isExist) {
                System.out.println("[NOT CORRECT]");
                System.out.println("User with that login doesn't exist.");
                writer.separateLines();
                return;
            }
            user = userService.findByLogin(login);
        }
        System.out.println("Profile information: ");
        String name = user.getName();
        boolean nameIsExist = name != null && !name.isEmpty();
        if (nameIsExist) {
            System.out.println("Name: " + name);
        }
        String login = user.getLogin();
        boolean loginIsExist = login != null && !login.isEmpty();
        if (loginIsExist) {
            System.out.println("Login: " + login);
        }
        if (!nameIsExist && !loginIsExist) {
            System.out.println("Profile has no information.");
            writer.separateLines();
            return;
        }
        Date dateStart = user.getDateStart();
        if (dateStart != null) {
            System.out.println("Registration date: " + Format.dateFormat(dateStart));
        }
    }

}
