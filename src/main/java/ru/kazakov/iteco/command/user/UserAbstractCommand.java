package ru.kazakov.iteco.command.user;

import ru.kazakov.iteco.command.AbstractCommand;
import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.service.UserService;
import ru.kazakov.iteco.view.ConsoleWriter;

public abstract class UserAbstractCommand extends AbstractCommand {

    UserService userService = bootstrap.getUserService();

    final ConsoleWriter writer = new ConsoleWriter();

    public UserAbstractCommand(Bootstrap bootstrap) {super(bootstrap);}

    @Override
    public abstract String getName();

    @Override
    public abstract String getDescription();

    @Override
    public abstract void setBootStrap(Bootstrap bootStrap);

    @Override
    public abstract void execute() throws Exception;

}
