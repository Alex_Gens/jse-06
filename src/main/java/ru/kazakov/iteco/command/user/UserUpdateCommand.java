package ru.kazakov.iteco.command.user;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.util.ConsoleReader;

public class UserUpdateCommand extends UserAbstractCommand {

    private final String name = "user-update";

    private final String description = "Update user name in profile";

    private final ConsoleReader reader = new ConsoleReader();

    public UserUpdateCommand(Bootstrap bootstrap) {super(bootstrap);}

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        User currentUser = bootstrap.getCurrentUser();
        User user = bootstrap.getCurrentUser();
        if (currentUser.getRoleType() == RoleType.ADMINISTRATOR) {
            System.out.println("Enter user's login to update user's profile.   [" + user.getRoleType().getName().toUpperCase() + "]");
            System.out.println("ENTER LOGIN: ");
            String login = reader.enterIgnoreEmpty();
            boolean isExist = userService.contains(login);
            if (!isExist) {
                System.out.println("[NOT CORRECT]");
                System.out.println("User with that login doesn't exist.");
                writer.separateLines();
                return;
            }
            user = userService.findByLogin(login);
        }
        System.out.println("ENTER NAME: ");
        String userName = reader.enterIgnoreEmpty();
        user.setName(userName);
        System.out.println("[UPDATED]");
        System.out.println("Name successfully updated!");
        writer.separateLines();
    }

}
