package ru.kazakov.iteco.command.user;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.util.ConsoleReader;
import ru.kazakov.iteco.util.Password;

public class UserCreateCommand extends UserAbstractCommand {

    private final String name = "user-create";

    private final String description = "New user Registration.";

    private final ConsoleReader reader = new ConsoleReader();

    public UserCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
        this.secure = false;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Registration new profile.");
        System.out.println("ENTER LOGIN ");
        String login = reader.enterIgnoreEmpty();
        while (userService.contains(login)) {
            System.out.println("Login is already exist. Use another login.");
            writer.separateLines();
            login = reader.enterIgnoreEmpty();
        }
        System.out.println("[CORRECT]");
        writer.separateLines();
        User user = new User();
        user.setLogin(login);
        String firstPassword = "";
        String secondPassword = "";
        while (true) {
            System.out.println("ENTER PASSWORD: ");
            firstPassword = reader.enterIgnoreEmpty();
            System.out.println("Confirm you password.");
            System.out.println("ENTER PASSWORD: ");
            secondPassword = reader.enterIgnoreEmpty();
            if (!firstPassword.equals(secondPassword)) {
                System.out.println("[NOT CORRECT]");
                System.out.println("Entered passwords are different.");
                writer.separateLines();
                continue;
            }
            System.out.println("[CORRECT]");
            System.out.println("Profile created!");
            writer.separateLines();
            break;
        }
        String password = Password.getHashedPassword(firstPassword);
        user.setPassword(password);
        userService.persist(user);
    }

}
