package ru.kazakov.iteco.command.system;

import ru.kazakov.iteco.command.AbstractCommand;
import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.view.ConsoleWriter;
import java.util.List;

public class HelpCommand extends AbstractCommand {

    private final ConsoleWriter writer = new ConsoleWriter();

    private final String name = "help";

    private final String description = "Show all commands.";

    public HelpCommand(Bootstrap bootstrap) {
        super(bootstrap);
        this.secure = false;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() {
        User currentUser = bootstrap.getCurrentUser();
        List<AbstractCommand> commands = bootstrap.getCommands();
        if (currentUser == null || currentUser.getRoleType() != RoleType.ADMINISTRATOR) {
            commands.forEach(v -> {
                if (!v.isAdmin()){
                    System.out.println(v.getName() + ": " + v.getDescription());
                }});
            writer.separateLines();
            return;
        }
        commands.forEach(v -> {
                System.out.println(v.getName() + ": " + v.getDescription());});
        writer.separateLines();
    }

}
