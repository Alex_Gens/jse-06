package ru.kazakov.iteco.command.system;

import ru.kazakov.iteco.command.AbstractCommand;
import ru.kazakov.iteco.context.Bootstrap;

public class ExitCommand extends AbstractCommand {

    private final String name = "exit";

    private final String description = "Close task manager.";

    public ExitCommand(Bootstrap bootstrap) {
        super(bootstrap);
        this.secure = false;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() {
        System.out.println("*** MANAGER CLOSED ***");
        System.exit(0);
    }

}
