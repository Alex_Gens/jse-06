package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;

public abstract class AbstractCommand {

    protected Bootstrap bootstrap;

    protected boolean secure = true;

    protected boolean admin = false;

    public AbstractCommand(Bootstrap bootstrap){
        this.bootstrap = bootstrap;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void setBootStrap(Bootstrap bootStrap);

    public abstract void execute() throws Exception;

    public boolean isSecure() {return secure;};

    public boolean isAdmin() {return admin;}

}
