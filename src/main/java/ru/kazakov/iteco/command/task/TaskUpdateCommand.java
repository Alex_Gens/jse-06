package ru.kazakov.iteco.command.task;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.util.ConsoleReader;

public class TaskUpdateCommand extends TaskAbstractCommand {

    private final ConsoleReader reader = new ConsoleReader();

    private final String name = "task-update";

    private final String description = "Update task information.";

    public TaskUpdateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        String name = writer.enterEntityName(entityType);
        if (!taskService.contains(name, currentUserId)) {
            System.out.println("Task with this name doesn't exist. Use \"task-list\" to show all tasks.");
            writer.separateLines();
            return;
        }
        Task task = taskService.findByName(name, currentUserId);
        System.out.println("Use \"-save\" to finish entering, and save information.");
        System.out.println("ENTER TASK INFORMATION");
        String newInfo = reader.read();
        task.setInfo(newInfo);
        System.out.println("[UPDATED]");
        System.out.println("Task successfully updated!");
        writer.separateLines();
    }

}
