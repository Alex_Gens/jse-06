package ru.kazakov.iteco.command.task;

import ru.kazakov.iteco.command.AbstractCommand;
import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.enumeration.EntityType;
import ru.kazakov.iteco.service.TaskService;
import ru.kazakov.iteco.view.ConsoleWriter;

public abstract class TaskAbstractCommand  extends AbstractCommand {

    final EntityType entityType = EntityType.TASK;

    final ConsoleWriter writer = new ConsoleWriter();

    final TaskService taskService = bootstrap.getTaskService();

    String currentUserId = "";

    public TaskAbstractCommand(Bootstrap bootstrap) {super(bootstrap); }

    public void execute() throws Exception {
        String currentUserId = bootstrap.getCurrentUser().getId();
        if (currentUserId == null || currentUserId.isEmpty()) {
            throw new Exception();
        }
        this.currentUserId = currentUserId;
    }

}
