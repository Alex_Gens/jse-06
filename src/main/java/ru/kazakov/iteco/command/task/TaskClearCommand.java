package ru.kazakov.iteco.command.task;

import ru.kazakov.iteco.context.Bootstrap;

public class TaskClearCommand extends TaskAbstractCommand {

    private final String name = "task-clear";

    private final String description = "Remove all tasks.";

    public TaskClearCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        taskService.removeAll(currentUserId);
        System.out.println("[ALL TASKS REMOVED]");
        System.out.println("Tasks successfully removed!");
        writer.separateLines();
    }

}
