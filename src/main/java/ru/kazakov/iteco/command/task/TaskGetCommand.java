package ru.kazakov.iteco.command.task;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Task;

public class TaskGetCommand extends TaskAbstractCommand {

    private final String name = "task-get";

    private final String description = "Show all task information.";

    public TaskGetCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        String name = writer.enterEntityName(entityType);
        if (!taskService.contains(name, currentUserId)) {
            System.out.println("Task with this name doesn't exist. Use \"task-list\" to show all tasks.");
            writer.separateLines();
            return;
        }
        Task task = taskService.findByName(name, currentUserId);
        if (taskService.isEmpty(task.getId())) {
            System.out.println("Task is empty. Use \"task-update\" to update this task.");
            writer.separateLines();
            return;
        }
        System.out.println("[Task: " + name + "]");
        System.out.println(task.getInfo());
        writer.separateLines();
    }

}
