package ru.kazakov.iteco.command.task;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Task;

public class TaskCreateCommand extends TaskAbstractCommand {

    private final String name = "task-create";

    private final String description = "Create new task.";

    public TaskCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        String name = writer.enterEntityName(entityType);
        if (taskService.contains(name, currentUserId)) {
            System.out.println("[NOT CREATED]");
            System.out.println("Project with this name is already exists. Use another project name.");
            writer.separateLines();
            return;
        }
        Task task = new Task(name);
        task.setUserId(currentUserId);
        taskService.persist(task);
        System.out.println("[CREATED]");
        System.out.println("Task successfully created!");
        writer.separateLines();
    }

}
