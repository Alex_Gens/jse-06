package ru.kazakov.iteco.command.task;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Task;

public class TaskRemoveCommand extends TaskAbstractCommand {

    private final String name = "task-remove";

    private final String description = "Remove task.";

    public TaskRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        String name = writer.enterEntityName(entityType);
        if (!taskService.contains(name, currentUserId)) {
            System.out.println("Task with this name doesn't exist. Use \"task-list\" to show all tasks.");
            writer.separateLines();
            return;
        }
        Task task = taskService.findByName(name, currentUserId);
        taskService.remove(task.getId());
        System.out.println("[REMOVED]");
        System.out.println("Task successfully removed!");
        writer.separateLines();
    }

}
