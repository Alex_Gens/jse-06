package ru.kazakov.iteco.command.task;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Task;
import java.util.List;

public class TaskListCommand extends TaskAbstractCommand {

    private final String name = "task-list";

    private final String description = "Show all tasks.";

    public TaskListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        List<Task> tasks = taskService.findAll(currentUserId);
        if (tasks == null || tasks.isEmpty()) {
            System.out.println("Task list is empty. Use \"task-create\" to create task.");
            writer.separateLines();
            return;
        }
        System.out.println("[TASKS LIST]");
        int counter = 1;
        for (Task task : tasks) {
            System.out.println(counter + ". " + task.getName());
            counter++;
        }
        writer.separateLines();
    }

}
