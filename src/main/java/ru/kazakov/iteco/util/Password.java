package ru.kazakov.iteco.util;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Password {

    public static String getHashedPassword(String password) throws NoSuchAlgorithmException {
        final String salt = "@JF27$o%";
        String hashed = password;
        for (int i = 0; i < 60000 ; i++) {
            hashed = salt + hashed + salt;
            hashed = md5Custom(hashed);
        }
        return hashed;
    }

    private static String md5Custom(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest).toLowerCase();
    }

}
