package ru.kazakov.iteco.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Format {

    public static String firstUpperCase(String str) {
        String result = "";
        if (str != null && !str.isEmpty()) {
            result = str.substring(0, 1).toUpperCase() + str.substring(1);
        }
        return result;
    }

    public static String dateFormat(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return dateFormat.format(date);
    }

}
