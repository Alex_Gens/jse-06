package ru.kazakov.iteco.util;

import ru.kazakov.iteco.view.ConsoleWriter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleReader {

    public BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public String enterIgnoreEmpty() throws IOException {
        String name = reader.readLine();
        while (name == null || name.isEmpty()) {
            name = reader.readLine();
        }
        return name;
    }

    public String read() throws IOException {
        StringBuilder builder = new StringBuilder().append("");
        String temp = reader.readLine();
        while (!temp.equals("-save")) {
            builder.append(temp);
            builder.append(new ConsoleWriter().separator);
            temp = reader.readLine();
        }
        return builder.toString();
    }

}
