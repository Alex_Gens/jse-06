package ru.kazakov.iteco.entity;

import ru.kazakov.iteco.api.IEntity;
import ru.kazakov.iteco.enumeration.RoleType;
import java.util.Date;
import java.util.UUID;

public class User implements IEntity {

    private final String id = UUID.randomUUID().toString();

    private String name = "";

    private Date dateStart = new Date();

    private Date dateFinish = new Date();

    private String login = "";

    private String password = "";

    private RoleType roleType = RoleType.DEFAULT;

    @Override
    public String getId() { return id;}

    public String getName() { return name;}

    public void setName(String name) { this.name = name;}

    @Override
    public Date getDateStart() { return dateStart;}

    @Override
    public void setDateStart(Date dateStart) {this.dateStart = dateStart; }

    @Override
    public Date getDateFinish() { return dateFinish;}

    @Override
    public void setDateFinish(Date dateFinish) {this.dateFinish = dateFinish; }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

}
