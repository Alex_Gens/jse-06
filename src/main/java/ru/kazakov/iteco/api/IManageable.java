package ru.kazakov.iteco.api;

import java.util.Date;

public interface IManageable extends IEntity {

    public Date getDateStart();

    public Date getDateFinish();

    public String getInfo();

    public void setInfo(String info);

}

