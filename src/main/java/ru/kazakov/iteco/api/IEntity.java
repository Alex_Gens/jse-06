package ru.kazakov.iteco.api;

import java.util.Date;

public interface IEntity {

    public String getId();

    public Date getDateStart();

    public void setDateStart(Date dateStart);

    public Date getDateFinish();

    public void setDateFinish(Date dateFinish);

}
